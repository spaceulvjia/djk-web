import request from '@/utils/request'

// 查询首页装修列表
export function listOrders(query) {
  return request({
    url: '/web/orders/list',
    method: 'get',
    params: query
  })
}

// 查询首页装修详细
export function getOrders(id) {
  return request({
    url: '/web/orders/' + id,
    method: 'get'
  })
}

// 新增首页装修
export function addOrders(data) {
  return request({
    url: '/web/orders',
    method: 'post',
    data: data
  })
}

// 修改首页装修
export function updateOrders(data) {
  return request({
    url: '/web/orders',
    method: 'put',
    data: data
  })
}

// 删除首页装修
export function delOrders(id) {
  return request({
    url: '/web/orders/' + id,
    method: 'delete'
  })
}
