import request from '@/utils/request'

// 查询分佣方案配置列表
export function listScheme(query) {
  return request({
    url: '/web/scheme/list',
    method: 'get',
    params: query
  })
}

// 查询分佣方案配置详细
// export function getScheme(id) {
//   return request({
//     url: '/web/scheme/' + id,
//     method: 'get'
//   })
// }
export function getScheme(id) {
  return request({
    url: '/web/scheme/getAgtSchemeDetailsById',
    method: 'get',
    params: {
      agtSchemeId: id
    }
  })
}

// 新增分佣方案配置
export function addScheme(data) {
  return request({
    url: '/web/scheme',
    method: 'post',
    data: data
  })
}

// 修改分佣方案配置
export function updateScheme(data) {
  return request({
    url: '/web/scheme',
    method: 'put',
    data: data
  })
}

// 删除分佣方案配置
export function delScheme(id) {
  return request({
    url: '/web/scheme/' + id,
    method: 'delete'
  })
}

// 删除分佣方案配置关联商品
export function delSchemeProd(id) {
  return request({
    url: '/web/scmProd/' + id,
    method: 'delete'
  })
}
