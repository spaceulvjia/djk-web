import request from '@/utils/request'

// 查询提现流水列表
export function listFlow(query) {
  return request({
    url: '/web/flow/list',
    method: 'get',
    params: query
  })
}

// 查询提现流水详细
export function getFlow(id) {
  return request({
    url: '/web/flow/' + id,
    method: 'get'
  })
}

// 新增提现流水
export function addFlow(data) {
  return request({
    url: '/web/flow',
    method: 'post',
    data: data
  })
}

// 修改提现流水
export function updateFlow(data) {
  return request({
    url: '/web/flow',
    method: 'put',
    data: data
  })
}

// 删除提现流水
export function delFlow(id) {
  return request({
    url: '/web/flow/' + id,
    method: 'delete'
  })
}
