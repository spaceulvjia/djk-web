import request from '@/utils/request'

// 查询商城详查明细列表
export function listItems(query) {
  return request({
    url: '/web/orderItems/list',
    method: 'get',
    params: query
  })
}

// 查询商城详查明细详细
export function getItems(id) {
  return request({
    url: '/web/orderItems/' + id,
    method: 'get'
  })
}

// 新增商城详查明细
export function addItems(data) {
  return request({
    url: '/web/orderItems',
    method: 'post',
    data: data
  })
}

// 修改商城详查明细
export function updateItems(data) {
  return request({
    url: '/web/orderItems',
    method: 'put',
    data: data
  })
}

// 删除商城详查明细
export function delItems(id) {
  return request({
    url: '/web/orderItems/' + id,
    method: 'delete'
  })
}
