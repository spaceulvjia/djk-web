import request from '@/utils/request'

// 查询会员列表
export function listCust(query) {
  return request({
    url: '/web/cust/list',
    method: 'get',
    params: query
  })
}

// 查询会员详细
export function getCust(id) {
  return request({
    url: '/web/cust/' + id,
    method: 'get'
  })
}

// 新增会员
export function addCust(data) {
  return request({
    url: '/web/cust',
    method: 'post',
    data: data
  })
}

// 修改会员
export function updateCust(data) {
  return request({
    url: '/web/cust',
    method: 'put',
    data: data
  })
}

// 删除会员
export function delCust(id) {
  return request({
    url: '/web/cust/' + id,
    method: 'delete'
  })
}
