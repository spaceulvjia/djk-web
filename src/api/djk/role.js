import request from '@/utils/request'

// 查询前端菜单列表
export function menuList(query) {
  return request({
    url: '/web/menu/getMenuList',
    method: 'get',
    params: query
  })
}

// 查询前端角色信息列表
export function listRole(query) {
  return request({
    url: '/web/role/list',
    method: 'get',
    params: query
  })
}

// 查询前端角色信息详细
export function getRole(roleId) {
  return request({
    url: '/web/role/' + roleId,
    method: 'get'
  })
}

// 新增前端角色信息
export function addRole(data) {
  return request({
    url: '/web/role',
    method: 'post',
    data: data
  })
}

// 修改前端角色信息
export function updateRole(data) {
  return request({
    url: '/web/role',
    method: 'put',
    data: data
  })
}

// 删除前端角色信息
export function delRole(roleId) {
  return request({
    url: '/web/role/' + roleId,
    method: 'delete'
  })
}
