import request from "@/utils/request";

// 查询分佣明细列表
export function listAllot(query) {
  return request({
    url: "/web/allot/list",
    method: "get",
    params: query,
  });
}

// 查询分佣明细详细
export function getAllot(id) {
  return request({
    url: "/web/allot/" + id,
    method: "get",
  });
}

// 新增分佣明细
export function addAllot(data) {
  return request({
    url: "/web/allot",
    method: "post",
    data: data,
  });
}

// 修改分佣明细
export function updateAllot(data) {
  return request({
    url: "/web/allot",
    method: "put",
    data: data,
  });
}

// 删除分佣明细
export function delAllot(id) {
  return request({
    url: "/web/allot/" + id,
    method: "delete",
  });
}
