import request from '@/utils/request'

// 查询会员等级管理列表
export function listType(query) {
  return request({
    url: '/web/type/list',
    method: 'get',
    params: query
  })
}

// 查询会员等级管理详细
export function getType(id) {
  return request({
    url: '/web/type/' + id,
    method: 'get'
  })
}

// 新增会员等级管理
export function addType(data) {
  return request({
    url: '/web/type',
    method: 'post',
    data: data
  })
}

// 修改会员等级管理
export function updateType(data) {
  return request({
    url: '/web/type',
    method: 'put',
    data: data
  })
}

// 删除会员等级管理
export function delType(id) {
  return request({
    url: '/web/type/' + id,
    method: 'delete'
  })
}
