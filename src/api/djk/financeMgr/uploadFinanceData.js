import request from '@/utils/request'

// 查询财务报列表
export function listFinance(query) {
  return request({
    url: '/web/finance/list',
    method: 'get',
    params: query
  })
}

// 查询财务报详细
export function getFinance(id) {
  return request({
    url: '/web/finance/' + id,
    method: 'get'
  })
}

// 新增财务报
export function addFinance(data) {
  return request({
    url: '/web/finance',
    method: 'post',
    data: data
  })
}

// 修改财务报
export function updateFinance(data) {
  return request({
    url: '/web/finance',
    method: 'put',
    data: data
  })
}

// 删除财务报
export function delFinance(id) {
  return request({
    url: '/web/finance/' + id,
    method: 'delete'
  })
}
