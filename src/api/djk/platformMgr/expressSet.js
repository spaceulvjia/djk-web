import request from '@/utils/request'

// 查询快递运费模板列表
export function listTemplate(query) {
  return request({
    url: '/web/template/list',
    method: 'get',
    params: query
  })
}

// 获取快递运费模板详细信息
export function getTemplate(id) {
  return request({
    url: '/web/template/' + id,
    method: 'get'
  })
}

// 新增快递运费模板
export function addTemplate(data) {
  return request({
    url: '/web/template',
    method: 'post',
    data: data
  })
}

// 修改快递运费模板
export function updateTemplate(data) {
  return request({
    url: '/web/template',
    method: 'put',
    data: data
  })
}

//  删除快递运费模板
export function delTemplate(id) {
  return request({
    url: '/web/template/' + id,
    method: 'delete',
  })
}

// 获取省份地址信息
export function getProvince() {
  return request({
    url: '/web/express/getSysAddress',
    method: 'post',
    data: {
      sysAccount: "HEALTH"
    }
  })
}