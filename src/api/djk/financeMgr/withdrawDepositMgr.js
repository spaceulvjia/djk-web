import request from '@/utils/request'

// 查询用户提现信息列表
export function listGetcash(query) {
  return request({
    url: '/web/getcash/list',
    method: 'get',
    params: query
  })
}

// 查询用户提现信息详细
export function getGetcash(id) {
  return request({
    url: '/web/getcash/' + id,
    method: 'get'
  })
}

// 新增用户提现信息
export function addGetcash(data) {
  return request({
    url: '/web/getcash',
    method: 'post',
    data: data
  })
}

// 修改用户提现信息
export function updateGetcash(data) {
  return request({
    url: '/web/getcash',
    method: 'put',
    data: data
  })
}

// 删除用户提现信息
export function delGetcash(id) {
  return request({
    url: '/web/getcash/' + id,
    method: 'delete'
  })
}
