import request from '@/utils/request'

// 查询退款申请单列表
export function listRefund(query) {
  return request({
    url: '/web/refund/list',
    method: 'get',
    params: query
  })
}

// 查询退款申请单详细
export function getRefund(id) {
  return request({
    url: '/web/refund/' + id,
    method: 'get'
  })
}

// 新增退款申请单
export function addRefund(data) {
  return request({
    url: '/web/refund',
    method: 'post',
    data: data
  })
}

// 修改退款申请单
export function updateRefund(data) {
  return request({
    url: '/web/refund',
    method: 'put',
    data: data
  })
}

// 删除退款申请单
export function delRefund(id) {
  return request({
    url: '/web/refund/' + id,
    method: 'delete'
  })
}

// 修改退款申请单
export function sendBill(data) {
  return request({
    url: '/web/refund/sendBill',
    method: 'post',
    data: data
  })
}
