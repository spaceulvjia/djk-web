import request from '@/utils/request'

// 查询客户提货卡列表
export function listExtcard(query) {
  return request({
    url: '/web/extcard/list',
    method: 'get',
    params: query
  })
}

// 查询客户提货卡详细
export function getExtcard(id) {
  return request({
    url: '/web/extcard/' + id,
    method: 'get'
  })
}

// 新增客户提货卡
export function addExtcard(data) {
  return request({
    url: '/web/extcard',
    method: 'post',
    data: data
  })
}

// 修改客户提货卡
export function updateExtcard(data) {
  return request({
    url: '/web/extcard',
    method: 'put',
    data: data
  })
}

// 删除客户提货卡
export function delExtcard(id) {
  return request({
    url: '/web/extcard/' + id,
    method: 'delete'
  })
}
