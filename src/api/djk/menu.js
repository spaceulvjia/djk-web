import request from '@/utils/request'

// 查询前端菜单列表
export function listMenu(query) {
  return request({
    url: '/web/menu/list',
    method: 'get',
    params: query
  })
}

// 查询前端菜单详细
export function getMenu(id) {
  return request({
    url: '/web/menu/' + id,
    method: 'get'
  })
}

// 新增前端菜单
export function addMenu(data) {
  return request({
    url: '/web/menu',
    method: 'post',
    data: data
  })
}

// 修改前端菜单
export function updateMenu(data) {
  return request({
    url: '/web/menu',
    method: 'put',
    data: data
  })
}

// 删除前端菜单
export function delMenu(id) {
  return request({
    url: '/web/menu/' + id,
    method: 'delete'
  })
}
