import request from '@/utils/request'

// 查询会员存货流水列表
export function listStkflow(query) {
  return request({
    url: '/web/stkflow/list',
    method: 'get',
    params: query
  })
}

// 查询会员存货流水详细
export function getStkflow(id) {
  return request({
    url: '/web/stkflow/' + id,
    method: 'get'
  })
}

// 新增会员存货流水
export function addStkflow(data) {
  return request({
    url: '/web/stkflow',
    method: 'post',
    data: data
  })
}

// 修改会员存货流水
export function updateStkflow(data) {
  return request({
    url: '/web/stkflow',
    method: 'put',
    data: data
  })
}

// 删除会员存货流水
export function delStkflow(id) {
  return request({
    url: '/web/stkflow/' + id,
    method: 'delete'
  })
}
