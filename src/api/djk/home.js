import request from '@/utils/request'

// 首页控制台
export function homeInfo() {
  return request({
    url: '/web/home/getHomeInfo',
    method: 'post',
    data: {
      "sysAccount": "HEALTH"
    }
  })
}

