import request from '@/utils/request'

// 查询培训中心列表
export function listInfo(query) {
  return request({
    url: '/web/info/list',
    method: 'get',
    params: query
  })
}

// 查询培训中心详细
export function getInfo(id) {
  return request({
    url: '/web/info/' + id,
    method: 'get'
  })
}

// 新增培训中心
export function addInfo(data) {
  return request({
    url: '/web/info',
    method: 'post',
    data: data
  })
}

// 修改培训中心
export function updateInfo(data) {
  return request({
    url: '/web/info',
    method: 'put',
    data: data
  })
}

// 删除培训中心
export function delInfo(id) {
  return request({
    url: '/web/info/' + id,
    method: 'delete'
  })
}
