import request from '@/utils/request'

// 查询系统资源列表
export function listResource(query) {
  return request({
    url: '/system/resource/list',
    method: 'get',
    params: query
  })
}

// 查询系统资源详细
export function getResource(Id) {
  return request({
    url: '/system/resource/' + Id,
    method: 'get'
  })
}

// 新增系统资源
export function addResource(data) {
  return request({
    url: '/system/resource',
    method: 'post',
    data: data
  })
}

// 修改系统资源
export function updateResource(data) {
  return request({
    url: '/system/resource',
    method: 'put',
    data: data
  })
}

// 删除系统资源
export function delResource(Id) {
  return request({
    url: '/system/resource/' + Id,
    method: 'delete'
  })
}
