import request from '@/utils/request'

// 查询商城订单列表
export function listOrders(query) {
  return request({
    url: '/web/orders/list',
    method: 'get',
    params: query
  })
}

// 查询商城订单详细
export function getOrders(id) {
  return request({
    url: '/web/orders/' + id,
    method: 'get'
  })
}

// 新增商城订单
export function addOrders(data) {
  return request({
    url: '/web/orders',
    method: 'post',
    data: data
  })
}

// 修改商城订单
export function updateOrders(data) {
  return request({
    url: '/web/orders',
    method: 'put',
    data: data
  })
}

// 删除商城订单
export function delOrders(id) {
  return request({
    url: '/web/orders/' + id,
    method: 'delete'
  })
}

// 填写物流发货单
export function setExpNo(data) {
  return request({
    url: '/web/bill/setExpNo',
    method: 'post',
    data: data
  })
}

// 查询物流信息
export function getExpress(data) {
  return request({
    url: '/web/orders/getExpress',
    method: 'post',
    data: data
  })
}