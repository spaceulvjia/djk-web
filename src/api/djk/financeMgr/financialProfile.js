import request from '@/utils/request'

// 财务概况
export function getFinancialProfile() {
  return request({
    url: '/web/home/getFinanceInfo',
    method: 'post',
    data: {
      "sysAccount": "HEALTH"
    }
  })
}