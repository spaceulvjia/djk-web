import request from '@/utils/request'

// 查询海报设置列表
export function listStage(query) {
  return request({
    url: '/web/stage/list',
    method: 'get',
    params: query
  })
}

// 查询海报设置详细
export function getStage(id) {
  return request({
    url: '/web/stage/' + id,
    method: 'get'
  })
}

// 新增海报设置
export function addStage(data) {
  return request({
    url: '/web/stage',
    method: 'post',
    data: data
  })
}

// 修改海报设置
export function updateStage(data) {
  return request({
    url: '/web/stage',
    method: 'put',
    data: data
  })
}

// 删除海报设置
export function delStage(id) {
  return request({
    url: '/web/stage/' + id,
    method: 'delete'
  })
}
