import request from '@/utils/request'

// 查询客户存货列表
export function listStock(query) {
  return request({
    url: '/web/stock/list',
    method: 'get',
    params: query
  })
}

// 查询客户存货详细
export function getStock(id) {
  return request({
    url: '/web/stock/' + id,
    method: 'get'
  })
}

// 新增客户存货
export function addStock(data) {
  return request({
    url: '/web/stock',
    method: 'post',
    data: data
  })
}

// 修改客户存货
export function updateStock(data) {
  return request({
    url: '/web/stock',
    method: 'put',
    data: data
  })
}

// 删除客户存货
export function delStock(id) {
  return request({
    url: '/web/stock/' + id,
    method: 'delete'
  })
}
